# README #

Welcome to my mobile game project repository!

### What will the game be about? ###

Basically you are a mage in search of an exit from a weird and wonderful dungeon. Collecting elements and unleashing spells on your enemies!

### Why this repo? ###

I want to store and showcase my project, also it's an easy way to get feedback from way better coders than I am! So if you feel something's not right, let me know!

### Updates ###

* 2016/11/29 Early prototype phase. Busy getting the basic gameplay down :-)