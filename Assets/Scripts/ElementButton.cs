﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementButton : MonoBehaviour {

    public int typeNum;

    public void ElementChosen()
    {
        Player.S.SelectElement(typeNum);
    }
}
