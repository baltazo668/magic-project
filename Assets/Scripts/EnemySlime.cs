﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlime : MonoBehaviour, IEnemy {

    // Implementing IEnemy properties
    [SerializeField]
    private float _touchDamage = 1;
    public float touchDamage
    {
        get { return (_touchDamage); }
        set { _touchDamage = value; }
    }

    private Vector3 _pos = Vector3.zero;
    public Vector3 pos
    {
        get { return (_pos); }
        set { _pos = value; }
    } 

    public float speed = 0.5f;
    public float health = 10;
    public float damageScale = 0.8f;
    public float damageScaleDuration = 0.25f;

    private float damageScaleStartTime;
    private float _maxHealth;
    private bool walking;
    private Transform characterTrans;

    // Stores damage for each element each frame
    public Dictionary<ElementType, float> damageDict;

    private SkinnedMeshRenderer skin;
    private UnityEngine.AI.NavMeshAgent nav;
    private Animator anim;
    private Rigidbody rb;

    void Awake()
    {
        characterTrans = transform.Find("CharacterTrans");
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        skin = GetComponentInChildren<SkinnedMeshRenderer>();
        _maxHealth = health; // Used to put a top cap on healing
        ResetDamageDict();
    }

    void Start()
    {
        Color slimeColor = Color.black; // Assign a random color to the slime
        while(slimeColor.r + slimeColor.g + slimeColor.b < 1)
        {
            slimeColor += new Color(Random.value, Random.value, Random.value);
        }
        skin.material.color = slimeColor;
        anim.SetBool("walking", true);
        nav.SetDestination(Player.S.transform.position);
        walking = true;
    }

    void ResetDamageDict()
    {
        if (damageDict == null)
        {
            damageDict = new Dictionary<ElementType, float>();
        }
        damageDict.Clear();
        damageDict.Add(ElementType.earth, 0);
        damageDict.Add(ElementType.water, 0);
        damageDict.Add(ElementType.air, 0);
        damageDict.Add(ElementType.fire, 0);
        damageDict.Add(ElementType.none, 0);
    }

    void Update()
    {

        if (walking) nav.SetDestination(Player.S.transform.position);

    }
    

    /* Damage this instance. By default, the damage is instant, but it can also be treated as damage
     * over time, where the amt value would be the amount of damage done every second.
     * NOTE: This same code can be used to heal the instance */
    public void Damage(float amt, ElementType eT, bool damageOverTime = false)
    {
        // If it's D.O.T. then only damage the fractionnal amount for this frame
        if (damageOverTime)
        {
            amt *= Time.deltaTime;
        }

        switch (eT)
        {
            case ElementType.fire:
                // Only the max damage from one fire source affects the instance
                damageDict[eT] = Mathf.Max(amt, damageDict[eT]);
                break;
            default:
                // Add any other damage
                damageDict[eT] += amt;
                break;
        }

    }

    void LateUpdate()
    {
        Debug.Log(health);
        if (health > 0)
        {
            // Apply damage from the different element types 
            float dmg = 0;
            foreach (KeyValuePair<ElementType, float> entry in damageDict)
            {
                dmg += entry.Value;
            }

            if (dmg > 0)
            {
                if (characterTrans.localScale == Vector3.one)
                {
                    damageScaleStartTime = Time.time;
                }
            }

            // The damage scale animation
            float damU = (Time.time - damageScaleStartTime) / damageScaleDuration;
            damU = Mathf.Min(1, damU); // Limit the max localScale to 1
            float scl = (1 - damU) * damageScale + damU * 1;
            characterTrans.localScale = scl * Vector3.one;

            health -= dmg;
            health = Mathf.Min(_maxHealth, health); // Limit health if healing
            ResetDamageDict(); // Prepare for the next frame
        }
         else if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        walking = false;
        nav.enabled = false;
        anim.SetTrigger("death");
        Destroy(gameObject, 2f);
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject go = collision.gameObject;
        Debug.Log("Slime collided with " + go.name);
        if(go.tag == "Player")
        {
            Attack();
        }
    }

    void Attack()
    {
        anim.SetTrigger("attack");
        Player.S.Damage(transform.position, touchDamage);
    }
}
