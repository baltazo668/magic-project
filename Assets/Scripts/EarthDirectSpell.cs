﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthDirectSpell : MonoBehaviour {

    public float duration = 2;

    // These are the % of the duration where the crystal is either moving up or down
    public float moveUpTime = 0.25f;
    public float moveDownTime = 0.75f;
    public float moveSpeed = 2f;

    public float damage = 2f;

    private float timeStart; // Birth time of this game object
    private float moveDownStart; // When to start moving back down
    private float moveDist = 2;
    private Vector3 startPos;
    private Vector3 upPos;


    void Start()
    {
        timeStart = Time.time;
        startPos = transform.position;
        upPos = new Vector3(startPos.x, 0, startPos.z);
    }

    void Update()
    {
        // Determine a number between 0 and 1 that stores the percentage of duration that has passed
        float u = (Time.time - timeStart) / duration;

        if(u <= moveUpTime)
        {
            // Move up from the ground
            float distCovered = (Time.time - timeStart) * moveSpeed;
            float fracMove = distCovered / moveDist;
            transform.position = Vector3.Lerp(startPos, upPos, fracMove);
        }
        else if(u >= moveDownTime)
        {
            Debug.Log("Move Down");
            // Move back down
            float distCovered = (Time.time - (timeStart+1.75f)) * moveSpeed;
            float fracMove = distCovered / moveDist;
            transform.position = Vector3.Lerp(upPos, startPos, fracMove);
        }

        if( u > 1)
        {
            Destroy(gameObject);
        }
        
    }

    void OnCollisionEnter(Collision other)
    {
        EnemySlime recipient = other.gameObject.GetComponent<EnemySlime>();
        if (recipient != null)
        {
            recipient.Damage(damage, ElementType.earth);
        }
   
    }
}
