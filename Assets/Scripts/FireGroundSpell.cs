﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGroundSpell : MonoBehaviour {

    public float duration = 4;
    public float durationVariance = 0.5f; // This float allows a variation of +- 0.5f
    public float fadeTime = 1f;
    public float timeStart; // Birth time of this game object
    public float damagePerSeconds = 10;

    void Start()
    {
        timeStart = Time.time;
        duration = Random.Range(duration - durationVariance, duration + durationVariance);

    }

    void Update()
    {
        // Determine a number between 0 and 1 that stores the percentage of duration that has passed
        float u = (Time.time - timeStart) / duration;
        float fadePercent = 1 - (fadeTime / duration);
        if (u > fadePercent)
        {
            float u2 = (u - fadePercent) / (1 - fadePercent);
            Vector3 loc = transform.position;
            loc.y = u2 * 2;
            transform.position = loc;
        }

        if (u > 1)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // Announce when another object enters the collider
        GameObject go = Utils.FindTaggedParent(other.gameObject);
        if (go == null)
        {
            go = other.gameObject;
        }
        Utils.tr("Flame hit", go.name);
    }

    void OnTriggerStay(Collider other)
    {
        EnemySlime recipient = other.GetComponent<EnemySlime>(); //TODO adjust for different enemies
        if (recipient != null)
        {
            recipient.Damage(damagePerSeconds, ElementType.fire, true);
        }
    }

    //TODO: Actually damage the other object

}

