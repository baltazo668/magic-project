﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy {

    Vector3 pos { get; set; } // The Enemy's transform.position

    float touchDamage { get; set; } // Damage done by touching the Mage

    // These are already implemented by all MonoBehaviour subclasses
    GameObject gameObject { get; }
    Transform transform { get; }
}
