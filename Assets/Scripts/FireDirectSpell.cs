﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDirectSpell : MonoBehaviour {

    public float damage = 1f;
    public float speed = 10f;
    public GameObject explosionPrefab;

    private GameObject enemy;
    private Vector3 attackTarget;


    public void Launch(GameObject target)
    {
        enemy = target;
        attackTarget = enemy.transform.position;
    }

    void LateUpdate()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, attackTarget, step);
    }

    void OnCollisionEnter(Collision other)
    {
        EnemySlime recipient = other.gameObject.GetComponent<EnemySlime>();
        if(recipient != null)
        {
            recipient.Damage(damage, ElementType.fire);
        }
        GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject, 0.1f);
    }
}
