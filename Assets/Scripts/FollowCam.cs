﻿using UnityEngine;

public class FollowCam : MonoBehaviour {

    public Transform target; // The target that the camera will follow
    public float smoothing = 5f; // The speed at which the camera will follow

    private Vector3 offset; // The offset from the target

    void Start()
    {
        offset = transform.position - target.position;
    }

    void FixedUpdate()
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.fixedDeltaTime);
    }
}
