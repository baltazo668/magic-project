﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The MPhase enum is used is used to track the phase of mouse interaction
public enum MPhase
{
    idle,
    down,
    drag
}

public enum ElementType
{
    earth,
    water,
    air,
    fire,
    none
}

// MouseInfo stores information about the mouse in each frame of interaction
[System.Serializable]
public class MouseInfo
{
    public Vector3 loc; // 3D loc of the mouse
    public Vector3 screenLoc; // Screen position of the mouse
    public Ray ray; // Ray from the mouse into 3D space
    public float time; // Time this MouseInfo was recorded
    public RaycastHit hitInfo; // Info about what was hit by the ray
    public bool hit; // Wether the mouse was over any collider

    // These methods see if the mouseRay hits anything
    public RaycastHit RayCast()
    {
        hit = Physics.Raycast(ray, out hitInfo);
        return (hitInfo);
    }
    public RaycastHit RayCast(int mask)
    {
        hit = Physics.Raycast(ray, out hitInfo, mask);
        return (hitInfo);
    }


}

public class Player : MonoBehaviour {

    static public Player S; // This is a singleton
    static public bool DEBUG = false;

    public float mTapTime = 0.1f; // How long is considered a tap
    public float mDragDist = 5; // Min dist in pixels to be a drag
    public GameObject moveTargetPrefab;
    public int maxNumSelectedElements = 1; // Maximum number of elements that can be selected simultaneously
    public GameObject[] elementPrefabs;
    public float elementRotDist = 0.5f; // Radius of rotation
    public float elementRotSpeed = 0.5f;
    public Color[] elementColors;
    // These set the min and amx distance between two line points
    public float lineMinDelta = 1f;
    public float lineMaxDelta = 3f;
    public float lineMaxLength = 30f;
    public Transform playerTransform;
    public float speed = 3.5f;
    public float health = 20;

    public Transform directSpellSpawnPt;
    public GameObject fireGroundSpellPrefab;
    public GameObject fireDirectSpellPrefab;
    public GameObject earthDirectSpellPrefab;

    public float hitForce = 10;
    public float hitRadius = 1;

    protected LineRenderer liner; // Ref to the LineRenderer component
    protected float lineY = 0.1f; // Z depth of the line
    protected Transform spellAnchor; // The parent Transform for all spells

    private List<Element> selectedElements = new List<Element>();
    private List<Vector3> linePts = new List<Vector3>(); // Points to be shown in the line
    private float totalLineLength;
    private MPhase mPhase = MPhase.idle;
    private List<MouseInfo> mouseInfos = new List<MouseInfo>();
    private string actionStartTag; // ["Player", "Enemy", "Ground"]
    private Vector3 moveTarget;
    private UnityEngine.AI.NavMeshAgent nav;
    private Rigidbody rb;
    private Animator anim;
    private bool targetAquired = false;

    void Awake()
    {
        S = this;
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        liner = GetComponent<LineRenderer>();
        liner.enabled = false;
        mPhase = MPhase.idle;
        GameObject saGO = new GameObject("Spell Anchor");
        spellAnchor = saGO.transform;
    }

    void Update()
    {
        // Find whether mouse 0 was pressed or released this frame
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        bool b0Down = Input.GetMouseButtonDown(0);
        bool b0Up = Input.GetMouseButtonUp(0);

        // Handle all inputs here, except for inventory buttons
        /* There are only a few possible actions :
         * 1. Tap on the ground to move to that point
         * 2. Drag on the ground without a spell selected to move the Player
         * 3. Drag on the ground with spell to cast along the ground
         * 4. Tap on enemy to attack (or force-push if no spell selected)
        */

        if (mPhase == MPhase.idle) // If the mouse is idle
        {
            if (b0Down)
            {
                targetAquired = false;
                StopWalking();
                mouseInfos.Clear(); // Clear the mouse infos
                AddMouseInfo(); // Add a firt MouseInfo
                if (mouseInfos[0].hit) // If something was hit
                {
                    MouseDown(); // Call MouseDown()
                    mPhase = MPhase.down; // Set the MPhase
                }
            }
        }

        if (mPhase == MPhase.down) // If the mouse is down
        {
            AddMouseInfo(); // Add a MouseInfo for this frame
            if (b0Up) // The mouse button was released
            {
                MouseTap(); // This was a tap
                mPhase = MPhase.idle;
            }
            else if (Time.time - mouseInfos[0].time > mTapTime) // If it's been longer than a tap it may be a drag but it must also move a certain number of pixels
            {
                float dragDist = (lastMouseInfo.screenLoc - mouseInfos[0].screenLoc).magnitude;
                if (dragDist >= mDragDist)
                {
                    mPhase = MPhase.drag;
                }
            }
        }

        if (mPhase == MPhase.drag) // If the mouse is dragged
        {
            AddMouseInfo();
            if (b0Up) // The mouse button was released
            {
                MouseDragUp();
                mPhase = MPhase.idle;
            }
            else
            {
                MouseDrag(); // Still dragging
            }
        }

        if (targetAquired) TurnTowardTarget();

    }

    void LateUpdate()
    {
        OrbitSelectedElements();
    }

    void TurnTowardTarget()
    {
        if (mouseInfos[0].hitInfo.collider != null)
        {
            Vector3 relativePos = mouseInfos[0].hitInfo.collider.gameObject.transform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10f * Time.deltaTime);
        }
    }

    // Pulls info from the mouse, adds it to mouseInfos and returns it
    MouseInfo AddMouseInfo() 
    {
        MouseInfo mInfo = new MouseInfo();
        mInfo.screenLoc = Input.mousePosition;
        mInfo.loc = Utils.mouseLoc; // Gets the position of the mouse at z=0
        mInfo.ray = Utils.mouseRay; // Gets the Ray from the Main camera through the mouse pointer
        mInfo.time = Time.time;
        mInfo.RayCast(); // Default is to Raycast with no mask

        if (mouseInfos.Count == 0) // If this is the first MouseInfo
        {
            mouseInfos.Add(mInfo); // Add mInfo to mouseInfos
        }
        else
        {
            float lastTime = mouseInfos[mouseInfos.Count - 1].time;
            if (mInfo.time != lastTime) // If time has passed since the last mouseInfo
            {
                mouseInfos.Add(mInfo);
            }
        }
        return (mInfo);
    }

    public MouseInfo lastMouseInfo //DONE
    {
        get // Access to the latest mouseInfo
        {
            if (mouseInfos.Count == 0) return (null);
            return (mouseInfos[mouseInfos.Count - 1]);
        }
    }

    void MouseDown() // The mouse was pressed on something, it could be a tap or a drag //DONE
    {
        if (DEBUG) print("Mage.MouseDown()");

        GameObject clickedGO = mouseInfos[0].hitInfo.collider.gameObject;

        GameObject taggedParent = Utils.FindTaggedParent(clickedGO);
        if (taggedParent == null)
        {
            actionStartTag = "";
        }
        else
        {
            actionStartTag = taggedParent.tag; // This would be either "Mage", "Ground" or "Enemy"
        }
    }

    void MouseTap()
    {
        if (DEBUG) print("Mage.MouseTap()");

        switch (actionStartTag)
        {
            case "Mage":
                // Do nothing
                break;
            case "Ground":
                // Move to tapped point
                StopWalking();
                targetAquired = false;
                SetMoveTarget();
                break;
            case "Enemy":
                // Cast Direct spell if tap on Enemy
                CastDirectSpell();
                break;
        }
    }

    void MouseDrag()
    {
        if (DEBUG) print("Mage.MouseDrag");

        // Drag is meaningless unless the mouse started on the ground
        if (actionStartTag != "Ground") return;

        if (selectedElements.Count != 0)
        {
            StopWalking();
            // Place the ground spell, so we need to draw a line
            if (!anim.GetBool("groundAttack"))
            {
                anim.SetBool("groundAttack", true);
            }

            AddPointToLiner(mouseInfos[mouseInfos.Count - 1].hitInfo.point);
            Vector3 relativePos = linePts[linePts.Count-1] - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10f * Time.deltaTime);

        }
        
    }

    void MouseDragUp()
    {
        if (DEBUG) print("Mage.MouseDragUp");

        // Drag is meaningless unless the mouse started on the ground
        if (actionStartTag != "Ground") return;

        // Cast the ground spell
        anim.SetBool("groundAttack", false);
        CastGroundSpell();
        ClearLiner();
    }

    void CastGroundSpell()
    {
        // There is not a no element ground spell, so return
        if (selectedElements.Count == 0) return;

        // Because this version of the prototype allows only a single element to be selected, we can use the 0th element to pick the spell
        switch (selectedElements[0].type)
        {
            case ElementType.fire:
                GameObject fireGroundGO;
                foreach (Vector3 pt in linePts)
                {
                    fireGroundGO = Instantiate(fireGroundSpellPrefab) as GameObject;
                    fireGroundGO.transform.parent = spellAnchor;
                    fireGroundGO.transform.position = pt;
                }
                break;
                // TODO: Add other spell element
        }
        // Clear the selected element; they're consumed by the spell
        ClearElements();
    }

    void CastDirectSpell()
    {

        // There is not a no element ground spell, so return
        if (selectedElements.Count == 0) return; // TODO: Maybe attack with the staff

        targetAquired = true;
        anim.SetTrigger("directAttack");
        switch (selectedElements[0].type)
        {
            case ElementType.fire:
                GameObject fireDirectGO;
                fireDirectGO = Instantiate(fireDirectSpellPrefab, directSpellSpawnPt.position, Quaternion.identity, spellAnchor);
                fireDirectGO.GetComponent<FireDirectSpell>().Launch(mouseInfos[0].hitInfo.collider.gameObject);
                break;
            case ElementType.earth:
                //GameObject earthDirectGO;
                Vector3 pos = new Vector3(mouseInfos[0].hitInfo.point.x, -2f, mouseInfos[0].hitInfo.point.z);
                Instantiate(earthDirectSpellPrefab, pos, Quaternion.identity, spellAnchor);
                break;
                // TODO: Add other spell element
        }
        // Clear the selected element; they're consumed by the spell
        ClearElements();
    }

    void StopWalking()
    {
        GameObject[] oldMoveTarget = GameObject.FindGameObjectsWithTag("MoveTarget");
        if (oldMoveTarget != null)
        {
            foreach (GameObject mt in oldMoveTarget)
            {
                mt.GetComponent<TapIndicator>().Remove(); // If there is an old Move target, delete it
            }
        }
        nav.speed = 0;
        anim.SetBool("flying", false);
    }

    void SetMoveTarget()
    {
        moveTarget = mouseInfos[mouseInfos.Count - 1].hitInfo.point;
        nav.SetDestination(moveTarget);
        nav.speed = speed;
        GameObject mTarget = Instantiate(moveTargetPrefab) as GameObject;
        mTarget.transform.position = moveTarget;
        anim.SetBool("flying", true);
    }

    void OnTriggerEnter(Collider other)
    {
        GameObject go = other.gameObject;
        if(go.tag == "MoveTarget")
        {
            Debug.Log("Reached Move Target");
            StopWalking();
        }
    }

    public void SelectElement(int elIndex)
    {
        if (elIndex == 4)
        {
            ClearElements();
            return;
        }

        if (maxNumSelectedElements == 1)
        {
            ClearElements();
        }

        if (selectedElements.Count >= maxNumSelectedElements) return;

        GameObject go = Instantiate(elementPrefabs[elIndex], playerTransform.position, Quaternion.identity, playerTransform) as GameObject;
        Element el = go.GetComponent<Element>();

        selectedElements.Add(el);
    }

    // Clears all elements and destroy their gameobject
    public void ClearElements()
    {
        foreach (Element el in selectedElements)
        {
            Destroy(el.gameObject);
        }
        selectedElements.Clear();
    }

    void OrbitSelectedElements()
    {
        // If there are none selected, just return
        if (selectedElements.Count == 0) return;

        Element el;
        Vector3 vec;
        float theta0, theta;
        float tau = Mathf.PI * 2;

        // Divide the circle into the number of elements selected
        float rotPerElement = tau / selectedElements.Count;

        // The base rotation angle (theta0) is based on time
        theta0 = elementRotSpeed * Time.time * tau;

        for (int i = 0; i < selectedElements.Count; i++)
        {
            // Determine the rotation angle for each element
            theta = theta0 + i * rotPerElement;
            el = selectedElements[i];

            // Turn the angle into a unit vector(trigonometry :_( )
            vec = new Vector3(Mathf.Cos(theta), 0, Mathf.Sin(theta));

            // Multiply that unit vector by the ElementRotDist
            vec *= elementRotDist;

            // Raise the element to waist height
            vec.y = 1.5f;
            el.transform.localPosition = vec; // Set the position of Element_Sphere
        }
    }

    public void Damage(Vector3 enemyPos, float damage)
    {
        Debug.Log("Received Damage from : "+enemyPos+" for "+damage+" pts");
        health -= damage;
        if(health <= 0)
        {
            Die();
            return;
        }
        anim.SetTrigger("hit");
        rb.AddExplosionForce(hitForce, enemyPos, hitRadius);
        Invoke("ResetVelocity", 1f);
    }

    void ResetVelocity()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    void Die()
    {
        Debug.Log("Hit points = 0");
    }

    // =============== Line Renderer code =====================\\

    // Add new point to the line
    // Ignores the point if it's too close and adds extra points if it's too far
    void AddPointToLiner(Vector3 pt)
    {
        pt.y = lineY;
        //linePts.Add(pt);
        //UpdateLiner();

        // Always add the point if linePts is empty
        if (linePts.Count == 0)
        {
            linePts.Add(pt);
            totalLineLength = 0;
            return; // And wait for the second point to enable line renderer
        }

        // If line is too long already, return
        if (totalLineLength > lineMaxLength) return;

        // If there is a previous point (pt0), then find how far pt is from it
        Vector3 pt0 = linePts[linePts.Count - 1]; // Get the last point in linePts
        Vector3 dir = pt - pt0;
        float delta = dir.magnitude;
        dir.Normalize();

        totalLineLength += delta;

        // If it's less than the min distance don't add it
        if (delta < lineMinDelta) return;

        // If it's further than the max distance then add extra points
        if (delta > lineMaxDelta)
        {
            float numToAdd = Mathf.Ceil(delta / lineMaxDelta);
            float midDelta = delta / numToAdd;
            Vector3 ptMid;
            for (int i = 0; i < numToAdd; i++)
            {
                ptMid = pt0 + (dir * midDelta * i);
                linePts.Add(ptMid);
            }
        }

        linePts.Add(pt);
        UpdateLiner();
    }

    // Update the line renderer with new points
    public void UpdateLiner()
    {
        // Get the type of the selected elements
        int el = 0;

        switch(selectedElements[0].type)
        {
            case ElementType.fire:
                el = 2;
                break;
            case ElementType.earth:
                el = 0;
                break;
            case ElementType.water:
                el = 3;
                break;
            case ElementType.air:
                el = 1;
                break;
        }


        // Set the line color based on that type
        liner.startColor = elementColors[el];
        liner.endColor = elementColors[el];
        //liner.SetColors(elementColors[el], elementColors[el]);

        // Update the representation of the ground spell about to be cast
        liner.numPositions = linePts.Count;
        //liner.SetVertexCount(linePts.Count);  
        for (int i = 0; i < linePts.Count; i++)
        {
            liner.SetPositions(linePts.ToArray());
        }

        liner.enabled = true;
    }

    public void ClearLiner()
    {
        liner.enabled = false;
        linePts.Clear();
    }
}
