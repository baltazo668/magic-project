﻿using UnityEngine;

public class SelfDestruct : MonoBehaviour {

    public float timeToDestroy = 2f;

	void Start()
    {
        Destroy(gameObject, timeToDestroy);
    }
}
