﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapIndicator : MonoBehaviour {

    private Animator anim;
    private CapsuleCollider coll;

	void Awake ()
    {
        coll = GetComponent<CapsuleCollider>();
        anim = GetComponent<Animator>();
	}
	
	public void Remove()
    {
        coll.enabled = false;
        anim.SetTrigger("destroy");
        Destroy(gameObject, 1f);
    }
}
